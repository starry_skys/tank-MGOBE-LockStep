const {ccclass, property} = cc._decorator;

@ccclass
export default class WxApi {
    public static initCloud(envId){
        wx.cloud.init({
            env: envId,
            traceUser: true
        });
    }

    public static callFunction(funcName,params?){
        return new Promise((resolve, reject) => {
            wx.cloud.callFunction({
                name: funcName,
                data: params || {},
                success: res => {
                    resolve(res)
                    console.log(`[云函数 ${funcName}] 调用成功: `, res);
                },
                fail: err => {
                    resolve(null)
                    console.log(`[云函数 ${funcName}] 调用失败: `, err);
                }
            })
        })
    }
}



