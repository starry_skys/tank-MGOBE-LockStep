
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property
    speed: number = 10 ;

    // LIFE-CYCLE CALLBACKS:

    private anim:cc.Animation;
    private moveDir: number;

    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN,this.onKeyDown,this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP,this.onKeyUp,this);

        this.moveDir = null;
    }

    start () {
      
    }

    onKeyDown(event: cc.Event.EventKeyboard){
        //如果当前行走方向和上次行走方向相同，就继续播放当前的行走动画
        // if(event.keyCode!=null){ 
        //     this.moveDir = event.keyCode;
        //     return;
        // }
        
        this.moveDir = event.keyCode;
    }

    onKeyUp(event){
        if(event.keyCode == this.moveDir){
            this.moveDir = null;
        }
    }

    update (dt) {
        switch(this.moveDir){
            case cc.macro.KEY.w : 
            this.node.y += this.speed*dt;
            break;
        case cc.macro.KEY.s : 
            this.node.y -= this.speed*dt;
            break;
        case cc.macro.KEY.a : 
            this.node.x -= this.speed*dt;
            break;
        case cc.macro.KEY.d : 
            this.node.x += this.speed*dt;
            break;
        }
    }
}
