
export default class Alert {

    show(content: string, confirmCallback?: any, cancelCallback?: any){

        cc.loader.loadRes("prefabs/Alert", cc.Prefab, function (error, prefab) {
            if (error) {
                cc.error(error);
                return;
            }
            //实例化对象
            let alert: cc.Node = cc.instantiate(prefab);

            //获取子节点
            let contentLabel = cc.find("alertBackground/contentLabel", alert).getComponent(cc.Label);
            let enterButton = cc.find("alertBackground/enterButton", alert);
            let cancelButton = cc.find("alertBackground/cancelButton", alert);

            contentLabel.string = content;

            enterButton.on('click', ()=>{
                alert.destroy();
                if(confirmCallback){
                    confirmCallback();
                }
            }); 

            cancelButton.on('click', ()=>{
                alert.destroy();
                if(cancelCallback){
                    cancelCallback();
                }
            });

             // 父节点
            alert.parent = cc.find("Canvas");

        });
    }

}

