import Hero from "./Hero";
import Global from "./Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    //子弹速度
    speed: number = 0;
    //子弹弧度
    bulletRadian: number = 0;
    //射程
    range = 700;

    distance = 0;
    distanceX = 0;
    distanceY = 0;
    bulletPool: cc.NodePool //传进来的子弹池对象
    teamId: string = null; //子弹所属队伍
    heroMgr: Hero = null;

    startPos: cc.Vec2;
    endPos: cc.Vec2;
    walkTime: number = 0;
    totalTime: number = 0;


    initBullet(rotateRadian: number, bNomalDegree: number, hero: Hero) {
        this.heroMgr = hero;

        this.distance = 0;
        this.speed = 800;
        this.bulletRadian = rotateRadian;
        //设置子弹朝向
        let rotateDegree = cc.misc.radiansToDegrees(rotateRadian);
        this.node.rotation = bNomalDegree - rotateDegree;
    }

    start() {

    }

    //设置当前帧的最终位置
    updatePosition(posX, posY) {

        this.endPos = cc.v2(posX, posY);
        this.startPos = this.node.getPosition();
        // cc.log(this.endPos)
        // cc.log(this.startPos)
        let dir = this.endPos.sub(this.startPos);
        let len = dir.mag();
        this.totalTime = len / this.speed;
        // cc.log(this.totalTime)
        this.walkTime = 0;

    }

    update(dt) {

        this.node.x += this.speed * Math.cos(this.bulletRadian) * dt;
        this.node.y += this.speed * Math.sin(this.bulletRadian) * dt;

        this.distance += this.speed * dt;
        if (this.distance >= this.range || this.collisionHero(this.node.getBoundingBox())) {
            this.destroyBullet();
        }

        //插值子弹位置
        // let cPos = this.node.getPosition();
        // if(this.startPos == this.endPos || this.walkTime >= this.totalTime){
        //     return;
        // }
        // this.walkTime += dt;
        // if(this.walkTime >= this.totalTime){
        //     this.walkTime = this.totalTime;
        // }
        // this.startPos.lerp(this.endPos,this.walkTime/this.totalTime,cPos);
        // this.node.setPosition(cPos);
    }

    /**
     *  把节点回收到对象池里边并且销毁
     */
    destroyBullet() {
        if (this.node) {
            this.node.parent = null;
            this.bulletPool.put(this.node);
        }
    }

    /**
     * 检测是否和敌方玩家碰撞
     * @param rect 
     */
    collisionHero(rect: cc.Rect) {
        for (let i = 0; i < Global.playerList.length; i++) {
            let hero: cc.Node = Global.playerList[i];
            let heroMgr: Hero = hero.getComponent("Hero");
            //友军忽略
            if (heroMgr.isDie || heroMgr.teamId == this.teamId) {
                continue;
            }

            let boundingbox = hero.getBoundingBox();
            if (boundingbox.intersects(rect)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 对象池get获取对象是会调用此方法
     * cc.NodePool.get() 可以传入任意数量类型的参数，这些参数会被原样传递给 reuse 方法
     * 如可以传入创建bullet的对象池bulletPool，用于之后回收子弹。
     */
    reuse(bulletPool: cc.NodePool) {
        this.bulletPool = bulletPool;
    }
}
