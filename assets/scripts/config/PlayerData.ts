let tankType = cc.Enum({
    normal: 1,
    speed: 2,
    big: 3
});
var playerType = {
    self: 1,
    friend: 2,
    enemy: 3
};

export default class Data{
    static tankType =  tankType;
    static playerType = playerType
};