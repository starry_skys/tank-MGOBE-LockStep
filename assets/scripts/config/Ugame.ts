//游戏的一些配置参数
var ugame = {
    //玩家数据
    userData: null,

    //------------------------------------/
    getCurMission() {
        return ugame.userData.curMission;
    },

    setCurMission(curMission) {
        ugame.userData.curMission = curMission;
    },

    getCurWeaponIndex() {
        return ugame.userData.curWeaponIndex;
    },

    setCurWeaponIndex(curWeaponIndex) {
        ugame.userData.curWeaponIndex = curWeaponIndex;
    },

    getWeaponList() {
        return ugame.userData.weaponList;
    },

    //设置n号位置指向的武器编号
    setWeaponList(index, value) {
        ugame[index - 1] = value;
    },

    syncUserData(){
        var jsonData = JSON.stringify(ugame.userData);
        cc.sys.localStorage.setItem("userData",jsonData);
    }

    
}

function loadUserData() {
    let data = cc.sys.localStorage.getItem("userData");
    //本地存在玩家数据
    if (data) {
        ugame.userData = JSON.parse(data);
        return;
    }
    //不存在则初始化数据
    ugame.userData = {
        //当前持有的武器在几号位(1.手枪 2.散弹枪 3.机枪 4.炮枪 5.激光枪)
        curWeaponIndex: 1,
        //每个位置配备的具体武器编号(例如1号手枪位可以为1号沙鹰或2号左轮手枪)
        weaponList: [11, 12, -1, -1, -1],
        //当前游戏在第几关
        curMission: 1,

        //人物移动速度
        heroSpeed: 200,
        //人物生命值
        heroHealth: 100,
        //人物防御值
        heroDefence: 10,
        //手枪精通（可以增加手枪伤害）
        gunSkill: 0,
        //散弹枪精通
        shotgunSkill: 0,
        //机枪精通
        machineSkill: 0,
        //炮枪精通
        cannonSkill: 0,
        //激光枪精通
        laserSkill: 0,
    }

    ugame.syncUserData();
}

loadUserData();

export default ugame;