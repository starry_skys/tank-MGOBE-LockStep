
const {ccclass, property} = cc._decorator;

@ccclass
export default class Config {

    debug: true ; 
    isTestUser: false;  //是否显示测试用户
    platform: 2;  //0=安卓 1=IOS  2=微信小程序
    socketHost :'127.0.0.1';
    socketPort : "8888";
    v: '1.0';
    
     // http://192.168.0.102:8888    http://123.206.6.44:8888  https://www.starryskys.cn
    static server1: string = "https://www.starryskys.cn";
}
