import { GameInfo } from "./Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Utils {
    //获取两点间距离
    public static getDistance(pos1: cc.Vec2, pos2: cc.Vec2) {
        let sum = Math.pow(pos1.x - pos2.x,2) + Math.pow(pos1.y - pos2.y,2);
        return Math.sqrt(sum);
    }

    //弧度转角度
    public static toDegree(radian: number){
        return radian * 180 / Math.PI ;
    }

    //角度转弧度
    public static toRadian(degree: number){
        return degree * Math.PI / 180 ;
    }

    public static parseData(data) {
        if (CC_JSB) {
            console.log("native platform");
            return eval('('+data+')');
        }else{
            return data;
        }
    };

    public static getGameInfo(): GameInfo{
        let res: GameInfo = null;
        let gameInfo = cc.sys.localStorage.getItem("gameInfo");
        if(gameInfo){
            res = JSON.parse(gameInfo);
        }
        return res;
    }

    public static setGameInfo(gameInfo: GameInfo){
        if(gameInfo){
            let json = JSON.stringify(gameInfo);
            cc.sys.localStorage.setItem("gameInfo",json);
        }
    }

}