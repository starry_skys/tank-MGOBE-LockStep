import Global,{ GameInfo, Player }  from "./Global";
import "./MGOBE";
import Utils from "./Utils";
import Alert from "./Alert";

const { Listener, Room } = MGOBE;

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    onLoad() {
        if(!Global.alert){
            Global.alert = new Alert();
        }
       
        //先判断是否拿到玩家信息
        if (!Global.userInfo) {
            cc.director.loadScene("start_scene");
            return;
        }

        this.initSDK();
    }

    start() {

    }

    initSDK() {
        //如果Player的id存在的话，就说明已经初始化sdk
        if (Global.openId && MGOBE.Player.id) {
            return;
        }

        let gameInfo: GameInfo = Utils.getGameInfo();
        if (!gameInfo) {
            gameInfo = {
                gameId: "obg-3s99bdqw",  //游戏id
                openId: Global.openId,
                secretKey: "6db3a1e6f5f2426b7f80de206577e2063ded4833",  //游戏key
                server: "3s99bdqw.wxlagame.com",
            };
            //放入缓存中
            Utils.setGameInfo(gameInfo);
        }

        this.initListen(gameInfo);

    }

    /**
     * 初始化监听房间
     * @param gameInfo 
     */
    initListen(initGameInfo: GameInfo) {

        Global.gameId = initGameInfo.gameId;
        Global.secretKey = initGameInfo.secretKey;
        Global.server = initGameInfo.server;

        let gameInfo: MGOBE.types.GameInfoPara = {
            gameId: Global.gameId,
            openId: Global.openId,
            secretKey: Global.secretKey,
        };

        let config: MGOBE.types.ConfigPara = {
            url: Global.server,
            reconnectMaxTimes: 5,
            reconnectInterval: 4000,
            resendInterval: 2000,
            resendTimeout: 20000,
            isAutoRequestFrame: true,
        };

        Listener.init(gameInfo, config, event => {
            if (event.code === MGOBE.ErrCode.EC_OK) {
                cc.log("SDK初始化成功");
                // 初始化后才能添加房间监听
                Global.room = new Room();
                Listener.add(Global.room);
            } else {
                cc.log("初始化失败");
            }
        });

    }

    chooseMode(event: cc.Event, mode: string) {
        let num = parseInt(mode);
        //1V1
        if (num == 1) {
           Global.matchCode = "match-ch1js9sf";
        }else if (num == 2) {
            
        }
        Global.playerCount = 2 * num;
        cc.director.loadScene("choose_scene");
    }

    chooseMode_bak(event: cc.Event, mode: string) {
        let num = parseInt(mode);
        //1V1
        let maxPlayers: number = 0;
        let roomType: string = null;
        if (num == 1) {
            maxPlayers = 2;
            roomType = "1V1";
        }else if (num == 2) {
            maxPlayers = 4;
            roomType = "2V2";
        }

        const playerInfo = {
            name: Global.userInfo.nickName,
            customPlayerStatus: 0,
            customProfile: Global.userInfo.avatarUrl,
        };
    
        const createTeamRoomPara = {
            roomName: "房间名",
            maxPlayers: maxPlayers,
            roomType: roomType,
            isPrivate: false,
            customProperties: "WAIT",
            playerInfo:playerInfo,
            teamNumber: 2,
        };
    
        Global.room.createTeamRoom(createTeamRoomPara, event => {
            cc.log(event.code);
            if(event.code === MGOBE.ErrCode.EC_OK){
                cc.director.loadScene("choose_scene");
            }
            cc.log(event.data.roomInfo);
        });
    }

}
