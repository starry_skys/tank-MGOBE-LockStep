import Alert from "./Alert";
import GameState from "./GameState";

interface UserInfo {
	nickName: string,
	gender: number,
	language: string,
	city: string,
	province: string,
	country: string,
	avatarUrl: string,
}

class Global {
	/** 房间信息 */
	public room: MGOBE.Room;
	/** 用户信息 */
	public userInfo: UserInfo;
	/** 用户openid */
	public openId: string;
	/** 控制台游戏id */
	public gameId: string;
	/** 控制台游戏key */
	public secretKey: string;
	/** 控制台域名 */
	public server: string;
	/** 控制台玩家匹配规则 */
	public matchCode: string;

	/** 弹框提示 */
	public alert: Alert;
    /** 本地玩家player:object{playerId、playerType}*/
    public localPlayer: Player;
    /** 玩家node列表-object{number:cc.Node}*/
    public playerNodes: any;
    /** 玩家node数组 */
    public playerList: cc.Node[];
    /**房间内所有人的出生位置坐标-{number: cc.Vec2}*/
	public playerPos: any;
	/** 房间玩家总数量 */
	public playerCount: number;
	public state: GameState;

}

const global = new Global();

export default global;
 
export interface GameInfo { 
    gameId: string, 
    openId: string, 
    secretKey: string, 
    server: string, 
};

export interface Player { 
	playerId: string,
    angle: number,
    pos: any,
    teamId: string,
    roomId: string,
    isDie: boolean,
    moveSpeed: number, //移动速度，单位：像素
    totalBlood: number,
    blood: number,
    damage: number,
    defence: number,
    lv: number
};

export interface PlayerFrame{
    playerId: string,
    isMoving?: boolean,
    moveSpeed?: number,
	moveRadian?: number,
	posX?: number,
	posY?: number,
	rotateRadian: number,
	isShooting: boolean
}

