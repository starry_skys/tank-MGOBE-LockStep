import Utils from "../Utils";
import Hero from "../Hero";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    rotateStick: cc.Node = null;  //右摇杆子弹控制点

    rstickRadian: number = 0; //右摇杆弧度
    heroMgr: Hero;
    
    /**
     * 右摇杆初始旋转角度
     */
    rotateDegree: number;
    /**
     * 是否射击
     */
    isShooting: boolean = false;
    duration: number = 0.1; //炮头旋转间隔时间

    initRotate(heroMgr: Hero){
        this.heroMgr = heroMgr;
        this.rotateDegree = this.rotateStick.rotation;
    }

    start() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchEnd, this);
    }

    // update (dt) {}

    onTouchStart(event: cc.Event.EventTouch) {
        //转为父节点下的坐标
        let touchPos = this.node.parent.convertToNodeSpaceAR(event.getLocation());
        let distance = Utils.getDistance(touchPos, cc.v2(0, 0));
        let radius = this.node.width / 2;

        //右摇杆的弧度（-π,π]范围
        this.rstickRadian = Math.atan2(touchPos.y, touchPos.x);
        //弧度转角度
        let rstickRotation = cc.misc.radiansToDegrees(this.rstickRadian);
        if (distance < radius) {
            this.rotateStick.rotation = this.rotateDegree - rstickRotation;
            //炮头旋转角度,数学中的弧度和cocos中的弧度方向相反
            // let rotateTo = cc.rotateTo(this.duration,-rstickRotation);
            // let callFunc = cc.callFunc(()=>{
            //     this.isShooting = true;
            //     this.heroMgr.onShoot(this.isShooting);
            // },this);
           
            // this.heroMgr.setRotateRadian(this.rstickRadian);
            // this.heroMgr.setWeaponRotation(-rstickRotation);
            // this.heroMgr.setIsShooting(this.isShooting);
            // this.heroMgr.onShoot(this.isShooting);
            this.isShooting = false;
        }
    }

    onTouchMove(event: cc.Event.EventTouch) {
        //转为父节点下的坐标
        let touchPos = this.node.parent.convertToNodeSpaceAR(event.getLocation());
        let distance = Utils.getDistance(touchPos, cc.v2(0, 0));
        let radius = this.node.width / 2;

        //右摇杆的弧度（-π,π]范围
        this.rstickRadian = Math.atan2(touchPos.y, touchPos.x);
        //弧度转角度
        let rstickRotation = Utils.toDegree(this.rstickRadian);
        
        //摇杆旋转角度
        this.rotateStick.rotation = this.rotateDegree - rstickRotation;
        //炮头旋转角度
        // this.heroMgr.setWeaponRotation(-rstickRotation);
        // this.heroMgr.setRotateRadian(this.rstickRadian);
    }

    onTouchEnd(event: cc.Event.EventTouch){
        // cc.game.emit("shoot",this.isShooting);
        this.isShooting = true;
        // this.heroMgr.setIsShooting(this.isShooting);
        // this.heroMgr.terminateShoot(this.isShooting);
    }
}
