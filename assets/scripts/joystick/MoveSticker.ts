import Utils from "../Utils";
import Hero from "../Hero";
import Global, { PlayerFrame } from "../Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    moveStick: cc.Node = null;  //左摇杆控制点

    lstickRadian: number; //左摇杆弧度
    moveSpeed: number = 0;
    isMoving: boolean = false;
    heroMgr: Hero;
    isSended: number = 0;
    sendList: Array<object> = null; //存储指令顺序集合,格式[{}]
    totalTime: number = 0;
    nowTime: number = 0;

    onLoad() {
        // this.sendList = new Array();
        // this.totalTime = 0.066 //单位秒，66ms发送一次帧消息
    }

    initMove(heroMgr: Hero) {
        this.heroMgr = heroMgr;
        this.moveSpeed = heroMgr.moveSpeed;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchEnd, this);
    }

    update(dt) {
        // this.nowTime += dt;
        // if (this.nowTime >= this.totalTime) {
        //     let data = {
        //         playerId: MGOBE.Player.id,
        //         moveSpeed: this.moveSpeed,
        //         isMoving: this.isMoving,
        //         moveRadian: this.lstickRadian
        //     }
        //     Global.room.sendFrame({ data }, (event) => {
        //         if (event.code === MGOBE.ErrCode.EC_OK) {
        //             // this.sendList.push(data);
        //         }
        //     });
        //     this.nowTime = 0;
        // }
    }


    onTouchStart(event: cc.Event.EventTouch) {
        //转为父节点下的坐标
        let touchPos = this.node.parent.convertToNodeSpaceAR(event.getLocation());
        let distance = Utils.getDistance(touchPos, cc.v2(0, 0));
        let radius = this.node.width / 2;

        //左摇杆的弧度（-π,π]范围
        this.lstickRadian = Math.floor(Math.atan2(touchPos.y, touchPos.x) * 1000) / 1000;
        if (distance < radius) {
            this.moveStick.setPosition(touchPos);
        }
        // this.heroMgr.setMoveRadian(this.lstickRadian);
        // let data = {
        //     playerId: MGOBE.Player.id,
        //     moveSpeed: 200,
        //     isMoving: false,
        //     moveRadian: this.lstickRadian
        // }
        // if(this.isSended == 0){
        //     Global.room.sendFrame({data},()=>this.isSended = 1);
        // }

        // Global.room.sendFrame({data},(event)=>{
        //     if(event.code === MGOBE.ErrCode.EC_OK){
        //         this.sendList.push(data);
        //     }
        // });

        this.moveSpeed = 200;
        this.isMoving = false;
    }

    onTouchMove(event: cc.Event.EventTouch) {
        let touchPos = this.node.parent.convertToNodeSpaceAR(event.getLocation());
        let distance = Utils.getDistance(touchPos, cc.v2(0, 0));
        //摇杆大半径
        let radius = this.node.width / 2;

        //左摇杆的弧度（-π,π]范围
        this.lstickRadian = Math.floor(Math.atan2(touchPos.y, touchPos.x) * 1000) / 1000;
        if (distance < radius) {
            this.moveStick.setPosition(touchPos);
        } else { // 触摸位置超过摇杆范围时，更新摇杆角度
            let posX = radius * Math.cos(this.lstickRadian);
            let posY = radius * Math.sin(this.lstickRadian);
            this.moveStick.setPosition(posX, posY);
        }
        // this.heroMgr.setIsMoving(true);
        // this.heroMgr.setMoveRadian(this.lstickRadian);
        // const data: PlayerFrame = {
        //     playerId: MGOBE.Player.id,
        //     isMoving: true,
        //     moveSpeed: 200,
        //     moveRadian: this.lstickRadian
        // };
        // Global.room.sendFrame({data},(event)=>{
        //     if(event.code === MGOBE.ErrCode.EC_OK){
        //         this.sendList.push(data);
        //     }
        // });
        // if(this.isSended >= 1){
        //     Global.room.sendFrame({data},()=>this.isSended += 1);
        // }
        this.moveSpeed = 200;
        this.isMoving = true;
    }

    onTouchEnd(event: cc.Event.EventTouch) {
        this.moveStick.setPosition(cc.v2(0, 0));
        // this.heroMgr.setIsMoving(false);
        // let data = {
        //     playerId: MGOBE.Player.id, 
        //     isMoving: false,
        //     moveSpeed: 0,
        //     moveRadian: this.lstickRadian
        // }
        // Global.room.sendFrame({data},(event)=>{
        //     if(event.code === MGOBE.ErrCode.EC_OK){
        //         this.sendList.push(data);
        //         cc.log("帧集合")
        //         cc.log(this.sendList)
        //         this.sendList.splice(0,this.sendList.length)
        //     }
        // });

        // if(this.isSended >= 1){
        //     Global.room.sendFrame({data},()=>this.isSended = 0);
        // }
        this.moveSpeed = 200;
        this.isMoving = false;

    }


}
