declare module cust {
    export interface GameInfo { 
        gameId: string, 
        openId: string, 
        secretKey: string, 
        server: string, 
    };
}

