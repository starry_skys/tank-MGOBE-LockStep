declare var global: {
	/** 客户端socket */
	socket: SocketIOClient.Socket,
	/** 本地玩家player:object{playerID、playerType} */
	localPlayer: any,
	/** 玩家node列表-object{number:cc.Node} */
	playerNodes: any,
	/** 玩家node数组*/
	playerList: cc.Node[],
	/** userid，从服务器生成的 */
	userid: string,
	/** 房间内所有人的出生位置坐标-{number: cc.Vec2} */
	playerPos: any,
}
